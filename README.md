# README #

I've decided to create 2D endless game as the tecnical task.
I think it's worth mentioning that this is my first cocs2d project. :)

So, the main idea here is to keep Bluevie (main character's name) alive as long as possible.
Gravity affects Bluevie, but the player can control it :

- tap (or mouse click) on upper half of the screen will force Bluevie to move up  
- tap (or mouse click) on lower half of the screen will force Bluevie to move down

Obstacles above, behind and under Bluevie are solid so Bluevie can interact with them.
Bluevie has to avoid enemies on his way (Oil drops).
When Blueview hits enemy then it dies and the game overs.

Also, bombs are thrown from time to time, so Bluevie has to be careful.

The main idea, as it's said above, is to keep Bluevie alive as long as possible, but 
also, to earn as much coins as you can.

Special Gems appear from time to time and Bluevie must collect them to earn more score points.

Hope you'll find this game interesting.

The game was tested on iOS and MacOS. I didn't try it on any other platform.

Main things TODO next :
- Make things working on Android and PC
- Improve elements physics and game logic (e.g. when to fire new element, and new movement trajectories etc.)
- Add "super power" ( e.g when Gem is hit Bluevie is protected against any enemy for several seconds)
- Add new elements
- Add ability to pause the game

Clone the repo and hit Run in xCode project!
I'll appreciate your contribution.