//
//  HUtils.cpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#include "HUtils.h"

Sprite* HUtils::getEnemy(float iScale)
{
    auto enemy = getGameElement(eEnemy, iScale);
    
    return enemy;
}


cocos2d::Sprite* HUtils::getMainCharacter(float iScale)
{
    auto character = getGameElement(eCharacter, iScale);
    return character;
}

cocos2d::Sprite* HUtils::getCoin(float iScale)
{
    auto coin = getGameElement(eCoin, iScale);
    return coin;
}


cocos2d::Sprite* HUtils::getBomb(float iScale)
{
    auto bomb = getGameElement(eBomb, iScale);
    return bomb;
}


cocos2d::Sprite* HUtils::getGrassObstacle(float iScale)
{
    auto grass = getGameElement(eGrassObstacle, iScale);
    return grass;
}

cocos2d::Sprite* HUtils::getGem(float iScale)
{
    auto gem = getGameElement(eGem, iScale);
    return gem;
}

cocos2d::Sprite* HUtils::getGameElement(eGameElement iElement, float iScale)
{
    int catMask = 0;
    int colMask = 0;
    int contMask = 0;
    bool dynamic = false;
    float mass = 10;
    std::string name;
    
    switch(iElement)
    {
        case eEnemy:
        {
            catMask  = (int)ePhy_ElementCategory::eElement_Enemy;
            colMask  = (int)ePhy_ElementCategory::eElement_Character;
            contMask = (int)ePhy_ElementCategory::eElement_Character;
            name = kEnemyImg;
            break;
        }
        
        case eCharacter:
        {
            catMask =   (int)ePhy_ElementCategory::eElement_Character;
            colMask =   (int)ePhy_ElementCategory::eElement_Obstacle |
                        (int)ePhy_ElementCategory::eElement_Enemy |
                        (int)ePhy_ElementCategory::eElement_Coin  |
                        (int)ePhy_ElementCategory::eElement_Bomb  |
                        (int)ePhy_ElementCategory::eElement_Gem;
            contMask =  (int)ePhy_ElementCategory::eElement_Obstacle |
                        (int)ePhy_ElementCategory::eElement_Enemy |
                        (int)ePhy_ElementCategory::eElement_Coin  |
                        (int)ePhy_ElementCategory::eElement_Bomb  |
                        (int)ePhy_ElementCategory::eElement_Gem;
            name = kMainCharacterImg;
            dynamic = true;
            break;
        }
        
        case eCoin:
        {
            catMask = (int)ePhy_ElementCategory::eElement_Coin;
            colMask  = (int)ePhy_ElementCategory::eElement_Character;
            contMask = (int)ePhy_ElementCategory::eElement_Character;
            name = kCoinImg;
            mass = 0;
            break;
        }
            
        case eBomb:
        {
            catMask = (int)ePhy_ElementCategory::eElement_Bomb;
            colMask  = (int)ePhy_ElementCategory::eElement_Obstacle | (int)ePhy_ElementCategory::eElement_Character;
            contMask = (int)ePhy_ElementCategory::eElement_Obstacle | (int)ePhy_ElementCategory::eElement_Character;
            name = kBombImg;
            mass = 10;
            dynamic = true;
            break;
        }
            
        case eGrassObstacle:
        {
            catMask = (int)ePhy_ElementCategory::eElement_Obstacle;
            colMask  = (int)ePhy_ElementCategory::eElement_Character |
            (int)ePhy_ElementCategory::eElement_Bomb;
            contMask = (int)ePhy_ElementCategory::eElement_Character |
            (int)ePhy_ElementCategory::eElement_Bomb;
            name = kGrassObstacleImg;
            mass = 5;
            dynamic = false;
            break;
        }
            
        case eGem:
        {
            catMask  = (int)ePhy_ElementCategory::eElement_Gem;
            colMask  = (int)ePhy_ElementCategory::eElement_Character;
            contMask = (int)ePhy_ElementCategory::eElement_Character;
            name = kGemImg;
            mass = 0;
            break;
        }
    }
    
    
    auto element = Sprite::create(name);
    
    // 1
    auto elementSize = element->getContentSize();
    element->setScale(iScale);
    element->setPosition(Vec2(0, 0));
    
    
    auto physicsBody = PhysicsBody::createBox(Size(elementSize.width * (iScale * 0.8)  ,
                                                   elementSize.height * (iScale * 0.8) ),
                                              PhysicsMaterial(0.1f, 1.0f, 0.0f));
    // 2
    physicsBody->setDynamic(dynamic);
    // 3
    physicsBody->setCategoryBitmask(catMask);
    physicsBody->setCollisionBitmask(colMask);
    physicsBody->setContactTestBitmask(contMask);
    physicsBody->setMass(mass);
    element->setPhysicsBody(physicsBody);
    
    return element;
}

cocos2d::ui::Button* HUtils::getMenuButton(const std::string& iCaption)
{
    auto button = cocos2d::ui::Button::create(kButtonBlueImg, kButtonGreenImg);
    button->setScale(kScaleFactor);
    button->setTitleText(iCaption);
    button->setTitleFontName(kFontName_MarkerFelt);
    button->setTitleFontSize(56);
    button->setPosition(Vec2(0,0));
    return button;
}
