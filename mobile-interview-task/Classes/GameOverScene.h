//
//  GameOverScene.hpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#ifndef GameOverScene_h
#define GameOverScene_h

#include "cocos2d.h"


class GameOverScene : public cocos2d::Scene
{
    //typedef cocos2d::Scene Super;
public:
    // implement the "static create()" method manually
    CREATE_FUNC(GameOverScene);
    
    // scene initialisation
    virtual bool init();
    // scene shown
    virtual void onEnter();
    // scene hidden
    virtual void onExit();
    
    // called once per frame
    virtual void update( float delta );
    void startAgain(Ref* pSender);
    
private:
    cocos2d::Label*  mGameOverLabel;
};


#endif /* GameOverScene_hpp */
