//
//  ActionScene.cpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/7/17.
//
//

#include "ActionScene.h"
#include "HUtils.h"
#include "GameOverScene.h"

using namespace cocos2d;
// on "init" you need to initialize your instance
bool ActionScene::init()
{
    // super init first
    if ( !cocos2d::Scene::init() )
    {
        return false;
    }
    
    // get visible size of window
    Size visibleSize = Director::getInstance()->getVisibleSize();
    auto label1 = Label::createWithTTF("Start", "Marker Felt.ttf", 36);
    
    label1->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 100));
    
    this->addChild(label1);
    // done
    return true;
}

bool ActionScene::initWithPhysics()
{
    // super init first
    if ( !cocos2d::Scene::initWithPhysics() )
    {
        return false;
    }
    
    Size wSize= Director::getInstance()->getVisibleSize();;
    Size scrSize={kBackgroundImageWidth,kBackgroundImageHeight};
    
    Sprite* background1 = Sprite::create(kActionSceneBackgroundImg);
    background1->setAnchorPoint(Vec2(0,0));
    background1->setScaleX(wSize.width/scrSize.width);
    background1->setScaleY(wSize.height/scrSize.height);
    this->addChild(background1);
    
    
    this->getPhysicsWorld()->setGravity(Vec2(kHorizontalGravity,kVericalGravity));
    this->addCharacter();
    this->createBottom();
    
    // BOTTOM
    touchEventListener = EventListenerTouchOneByOne::create();
    touchEventListener->setEnabled(true);
    touchEventListener->onTouchBegan = std::bind(&ActionScene::onTouchBegin, this, std::placeholders::_1, std::placeholders::_2);
    
    
    contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = std::bind(&ActionScene::onContactBegan, this, std::placeholders::_1);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchEventListener, this);
    
    mPointsLabel = Label::createWithTTF("", "Marker Felt.ttf", 36);
    mPointsLabel->setPosition(Vec2(wSize.width - 150, wSize.height - 50));
    this->addChild(mPointsLabel);
    
    mLevelLabel = Label::createWithTTF("", "Marker Felt.ttf", 36);
    mLevelLabel->setPosition(Vec2(150, wSize.height - 50));
    this->addChild(mLevelLabel);
    
    updateLevelLabel();
    updatePointsLabel();
    mCharacterDied = false;
    // done
    return true;
}

void ActionScene::createBottom()
{
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    for(int i = 0; i < 100; ++i)
    {
        auto piece = HUtils::getGrassObstacle(0.1);//Sprite::create( "Leaf6.png" );
        piece->setPosition( Vec2( 50 * i, 20) );
        this->addChild( piece );
    }
    
    for(int i = 0; i < 100; ++i)
    {
        auto piece = HUtils::getGrassObstacle(0.1);
        piece->setRotation(180);
        piece->setPosition( Vec2( 50 * i, visibleSize.height) );
        this->addChild( piece );
    }
    
    for(int i = 0; i < 100; ++i)
    {
        auto piece = HUtils::getGrassObstacle(0.1);
        piece->setPosition( Vec2(0, i * 50) );
        piece->setRotation(90);
        piece->setScale(0.1, 0.1);
        this->addChild( piece );
    }
}

void ActionScene::addCharacter()
{
    // get visible size of window
    Size visibleSize = Director::getInstance()->getVisibleSize();

    // add a sprite
    mCharacter = HUtils::getMainCharacter(kScaleFactor);
    mCharacter->setPosition( Vec2( kMainCharacterStartingPointX, visibleSize.height / 2 ) );
    this->addChild( mCharacter );
    
}

void ActionScene::fireThisElement(Sprite* iElement, float iDurationConstant)
{
    auto elementContentSize = iElement->getContentSize();
    auto selfContentSize = this->getContentSize();
    int minY = elementContentSize.height/2;
    int maxY = selfContentSize.height - elementContentSize.height/2;
    int rangeY = maxY - minY;
    int randomY = (rand() % rangeY) + minY;
    iElement->setPosition(Vec2(selfContentSize.width + elementContentSize.width/2, randomY));
    
    this->addChild(iElement);
    // 2
    int minDuration = 2.0 * (iDurationConstant >= 0 ? iDurationConstant : 1);
    int maxDuration = 8.0 * ( iDurationConstant >= 0 ? iDurationConstant : 0.28);
    int rangeDuration = maxDuration - minDuration;
    int randomDuration = (rand() % rangeDuration) + minDuration;
    
    // 3
    auto actionMove = MoveTo::create(randomDuration, Vec2(-elementContentSize.width/2, randomY));
    auto actionRemove = RemoveSelf::create();
    iElement->runAction(Sequence::create(actionMove,actionRemove, nullptr));
}


void ActionScene::addBomb()
{
    Size sz = Director::getInstance()->getVisibleSize();
    auto bomb = HUtils::getBomb(kScaleFactor);
    bomb->setPosition(Vec2(sz.width , 200));
    int k = 5;
    this->addChild(bomb);
    
    auto selfContentSize = this->getContentSize();
    int minY = selfContentSize.height/4;
    int maxY = selfContentSize.height/1.5;
    
    int rangeY = maxY - minY;
    int randomY = (rand() % rangeY) + minY;
    
    bomb->getPhysicsBody()->applyImpulse(Vec2( -sz.width * k, k * randomY));
    
}
void ActionScene::addEnemy()
{
    auto enemy = HUtils::getEnemy(kScaleFactor);
    fireThisElement(enemy, (float)3/(float)mGameInfo.getLevel());
}

void ActionScene::addCoin()
{
    auto coin = HUtils::getCoin(kScaleFactor);
    fireThisElement(coin, 1);
}


void ActionScene::addGem()
{
    auto gem = HUtils::getGem(kScaleFactor);
    fireThisElement(gem, (float)3/(float)mGameInfo.getLevel());
}

void ActionScene::update( float delta )
{
    
    Scene::update(delta);
    // called once per frame
    //    cocos2d::log( "Update: %f", delta );
    if(mCounter.isEnemyTime())
    {
        addEnemy();
    }
    
    if(mCounter.isCoinTime())
    {
        addCoin();
    }
    
    if(mGameInfo.getLevel() >= 2 &&  mCounter.isBombTime())
    {
        addBomb();
    }
    int cycle = mCounter.getNumCycles();
    mCounter.increment();
    
    if(mCounter.getNumCycles() > cycle)
    {
        mGameInfo.increaseLevel();
        updateLevelLabel();
        addGem();
    }
    updateCharacterPosition();
    
}

bool ActionScene::onTouchBegin(cocos2d::Touch* iTouch, cocos2d::Event* iEvent)
{
    Vec2 tloc = iTouch->getLocation();
    int impulse = kTouchimpulse;
    int direction = 1;
    if(tloc.y < Director::getInstance()->getVisibleSize().height / 2)
        direction = -1;
    
    if(mCharacterDied)
        return true;
    
    mCharacter->getPhysicsBody()->applyImpulse(Vect(0, direction * impulse));
    return true;
}

bool ActionScene::onContactBegan(PhysicsContact &contact)
{
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    Node* charPtr = nullptr;
    Node* bombPtr = nullptr;
    Node* obstaclePtr = nullptr;
    Node* coinPtr = nullptr;
    Node* enemyPtr = nullptr;
    Node* gemPtr = nullptr;
    
    auto bodyA = contact.getShapeA()->getBody();
    auto bodyB = contact.getShapeB()->getBody();
    
    switch(bodyA->getCategoryBitmask())
    {
        case (int)ePhy_ElementCategory::eElement_Character:
        {
            charPtr = nodeA;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Coin:
        {
            coinPtr = nodeA;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Bomb:
        {
            bombPtr = nodeA;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Obstacle:
        {
            obstaclePtr = nodeA;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Enemy:
        {
            enemyPtr = nodeA;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Gem:
        {
            gemPtr = nodeA;
            break;
        }
    }
    
    switch(bodyB->getCategoryBitmask())
    {
        case (int)ePhy_ElementCategory::eElement_Character:
        {
            charPtr = nodeB;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Coin:
        {
            coinPtr = nodeB;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Bomb:
        {
            bombPtr = nodeB;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Obstacle:
        {
            obstaclePtr = nodeB;
            break;
        }
        case (int)ePhy_ElementCategory::eElement_Enemy:
        {
            enemyPtr = nodeB;
            break;
        }
            
        case (int)ePhy_ElementCategory::eElement_Gem:
        {
            gemPtr = nodeB;
            break;
        }
    }
    
    
    if(charPtr && enemyPtr)
        return handleCharacterEnemyCollision(enemyPtr);
    
    if(charPtr && bombPtr)
        return handleCharacterBombCollision(bombPtr);
    
    if(charPtr && coinPtr)
        return handleCharacterCoinCollision(coinPtr);
    
    if(charPtr && gemPtr)
        return handleCharacterGemCollision(gemPtr);
    
    if(bombPtr && obstaclePtr)
        return handleBombObstacleCollision(bombPtr, obstaclePtr);
    
    return true;
}

void ActionScene::updateCharacterPosition()
{
    if(mCharacterDied)
        return;
    Vec2 v = mCharacter->getPhysicsBody()->getVelocity() ;
    float rotation = mCharacter->getRotation();
    if(v.y < 0)
        mCharacter->setTexture(kMainCharacterFallinglImg);
    else
        mCharacter->setTexture(kMainCharacterImg);
    
    if(rotation != 0.0)
    {
        mCharacter->setRotation(0.0);
    }

}


bool ActionScene::handleCharacterGemCollision(Node* iGem)
{
    mGameInfo.addPoints(kGemPoints * mGameInfo.getLevel());
    iGem->removeFromParent();
    updatePointsLabel();
    return true;
}

bool ActionScene::handleCharacterCoinCollision(Node* iCoin)
{
    mGameInfo.addPoints(kCoinPoints);
    iCoin->removeFromParent();
    updatePointsLabel();
    return false;
}

bool ActionScene::handleCharacterBombCollision(Node* iBomb)
{
    Vec2 position = iBomb->getPosition();
    
    Sprite* dust = Sprite::create(kSmokeOrangeImg);
    dust->setPosition(position);
    dust->setScale(kScaleFactor);
    iBomb->removeFromParent();
    this->addChild(dust);
    
    Sprite* deadChar = Sprite::create(kMainCharacterFlatImg);
    deadChar->setScale(kScaleFactor);
    
    deadChar->setPosition(mCharacter->getPosition());
    
    mCharacter->removeFromParent();
    this->addChild(deadChar);
    mCharacterDied = true;
    
    auto delay = DelayTime::create(1.0);
    auto gmOver = CallFunc::create( std::bind(&ActionScene::gameOver, this));
    this->runAction(Sequence::create(delay, gmOver, NULL));
    
    return true;
}

bool ActionScene::handleCharacterEnemyCollision(Node* iEnemy)
{
    Vec2 position = iEnemy->getPosition();
    
    Sprite* dust = Sprite::create(kSmokeBrownImg);
    dust->setPosition(position);
    dust->setScale(kScaleFactor);
    iEnemy->removeFromParent();
    this->addChild(dust);
    
    Sprite* deadChar = Sprite::create(kMainCharacterFlatImg);
    deadChar->setScale(kScaleFactor);
    
    deadChar->setPosition(mCharacter->getPosition());
    
    mCharacter->removeFromParent();
    this->addChild(deadChar);
    mCharacterDied = true;
    
    auto delay = DelayTime::create(1.0);
    auto gmOver = CallFunc::create( std::bind(&ActionScene::gameOver, this));
    this->runAction(Sequence::create(delay, gmOver, NULL));
    
    return true;
}


bool ActionScene::handleBombObstacleCollision(Node* iBomb, Node* iObstacle)
{
    Vec2 position = iBomb->getPosition();
    Sprite* dust = Sprite::create(kSmokeOrangeImg);
    dust->setPosition(position);
    dust->setScale(kScaleFactor);
    iBomb->removeFromParent();
    this->addChild(dust);

    auto delayAction = DelayTime::create(0.5);
    auto removeSelf = RemoveSelf::create();
    
    dust->runAction(Sequence::create(delayAction, removeSelf, nullptr));
    
    return true;
}

void    ActionScene::updatePointsLabel()
{
    std::string str = kPointsLabelCaption;
    str = str.append(std::to_string(mGameInfo.getPoints()));
    mPointsLabel->setString(str);
}

void ActionScene::updateLevelLabel()
{
    std::string str = kLevelLabelCaption;
    str = str.append(std::to_string(mGameInfo.getLevel()));
    mLevelLabel->setString(str);
}

void ActionScene::gameOver()
{
    auto scene = GameOverScene::create();
    Director::getInstance()->resume();
    Director::getInstance()->replaceScene(TransitionFlipX::create(2, scene));
}

void ActionScene::onExit()
{
    Scene::onExit();
    
    // de-register event listeners
    _eventDispatcher->removeEventListener( touchEventListener );
    _eventDispatcher->removeEventListener( contactListener );
    
    // unschedule update
    unscheduleUpdate();
}

void ActionScene::onEnter()
{
    Scene::onEnter();
    Director::getInstance()->resume();
    // schedule update calls
    scheduleUpdate();
}