//
//  HCounte.hpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#ifndef HCounte_hpp
#define HCounte_hpp

class HCounter
{
public:
    HCounter();
    ~HCounter();
    
    void increment();
    bool isEnemyTime();
    bool isCoinTime();
    bool isBombTime();
    int getNumCycles() { return numCycles; };
    
    
private:
    int count;
    int numCycles;
};
#endif /* HCounte_hpp */
