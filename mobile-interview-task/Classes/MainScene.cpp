//
//  MainScene.cpp
//  ExpJam
//
//  Created by Two Tails on 09/12/2014.
//
//

#include "MainScene.h"
#include "ActionScene.h"
#include "HUtils.h"

#include <ui/CocosGUI.h>
USING_NS_CC;

// on "init" you need to initialize your instance
bool MainScene::init()
{
    // super init first
    if ( !Super::init() )
    {
        return false;
    }
    
    // get visible size of window
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Size scrSize={kBackgroundImageWidth,kBackgroundImageHeight};
    
    Sprite* background1 = Sprite::create(kActionSceneBackgroundImg);
    background1->setAnchorPoint(Vec2(0,0));
    background1->setScaleX(visibleSize.width/scrSize.width);
    background1->setScaleY(visibleSize.height/scrSize.height);
    this->addChild(background1);
    
    int offset = 50;
    // add a sprite
    Sprite* sprite = Sprite::create(kMainCharacterImg);
    sprite->setPosition( Vec2( visibleSize.width / 2, visibleSize.height / 2 + offset ) );
    // add the sprite as a child to this layer
    this->addChild( sprite );
    
    auto button = HUtils::getMenuButton(kStartGameMenuItem);
    button->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 100));
    button->addTouchEventListener( [this](Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
    {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
        {
            // do something
            this->startGame(pSender);
        }
    } );
    
    this->addChild(button);
    
    button = HUtils::getMenuButton(kExitGameMenuItem);
    button->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 180));
    button->addTouchEventListener( [this](Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
                                  {
                                      if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
                                      {
                                          // do something
                                          this->exitPlease(pSender);
                                      }
                                  } );
    this->addChild(button);
    
    
    // done
    return true;
}

bool MainScene::onTouchBegin(cocos2d::Touch*, cocos2d::Event*)
{
    startGame(nullptr);
    return true;
}

void MainScene::exitPlease(Ref* pSender)
{
    Director::getInstance()->end();
    exit(0);
}

void MainScene::startGame(Ref* pSender)
{
    mActionScene = ActionScene::createWithPhysics();
    Director::getInstance()->pushScene(mActionScene);
}

void MainScene::onEnter()
{
    Super::onEnter();
    
    // schedule update calls
    scheduleUpdate();
}

void MainScene::onExit()
{
    Super::onExit();
    
    // unschedule update
    unscheduleUpdate();
}

void MainScene::update( float delta )
{
    // called once per frame
//    cocos2d::log( "Update: %f", delta );
    
}
