//
//  GameOverScene.cpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#include "GameOverScene.h"

#include "MainScene.h"
#include "ActionScene.h"
#include "HUtils.h"

USING_NS_CC;



// on "init" you need to initialize your instance
bool GameOverScene::init()
{
    // super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    // get visible size of window
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Size scrSize={kBackgroundImageWidth,kBackgroundImageHeight};
    
    Sprite* background1 = Sprite::create(kActionSceneBackgroundImg);
    background1->setAnchorPoint(Vec2(0,0));
    background1->setScaleX(visibleSize.width/scrSize.width);
    background1->setScaleY(visibleSize.height/scrSize.height);
    this->addChild(background1);
    
    
    // add a sprite
    Sprite* sprite = Sprite::create( kMainCharacterRedNormalImg );
    sprite->setPosition( Vec2( visibleSize.width / 2, visibleSize.height / 2 ) );
    // add the sprite as a child to this layer
    this->addChild( sprite );
    
    mGameOverLabel = Label::createWithTTF(kGameOverText, kFontName_MarkerFelt, 56);
    mGameOverLabel->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2 + 120));
    mGameOverLabel->setTextColor(Color4B::RED);
    this->addChild(mGameOverLabel);
    
    auto button = HUtils::getMenuButton(kTryAgainGameMenuItem);
    button->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 130));
    button->addTouchEventListener( [this](Ref* pSender, cocos2d::ui::Widget::TouchEventType type)
                                  {
                                      if (type == cocos2d::ui::Widget::TouchEventType::ENDED)
                                      {
                                          // do something
                                          this->startAgain(pSender);
                                      }
                                  } );
    
    this->addChild(button);
    
    // done
    return true;
}

void GameOverScene::startAgain(Ref* pSender)
{
    Director::getInstance()->popToRootScene();
}

void GameOverScene::onEnter()
{
    Scene::onEnter();
    scheduleUpdate();
}

void GameOverScene::onExit()
{
    Scene::onExit();
    // unschedule update
    unscheduleUpdate();
}

void GameOverScene::update( float delta )
{
    // called once per frame
    //    cocos2d::log( "Update: %f", delta );
    
}


#pragma mark - Key Events
