//
//  HUtils.hpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#ifndef HUtils_hpp
#define HUtils_hpp

#include "cocos2d.h"
#include <ui/CocosGUI.h>

using namespace cocos2d;

class HUtils
{
public:
    enum eGameElement
    {
        eEnemy,
        eCharacter,
        eCoin,
        eBomb,
        eGem,
        eGrassObstacle,
    };
    
    static cocos2d::Sprite* getEnemy(float iScale);
    static cocos2d::Sprite* getMainCharacter(float iScale);
    static cocos2d::Sprite* getCoin(float iScale);
    static cocos2d::Sprite* getBomb(float iScale);
    static cocos2d::Sprite* getGrassObstacle(float iScale);
    static cocos2d::Sprite* getGem(float iScale);
    
    static cocos2d::ui::Button* getMenuButton(const std::string& iCaption);
    
private:
    
    
    static cocos2d::Sprite* getGameElement(eGameElement iElement, float iScale);
};

#endif /* HUtils_hpp */
