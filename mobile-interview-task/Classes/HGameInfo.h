//
//  HGameInfo.hpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#ifndef HGameInfo_hpp
#define HGameInfo_hpp

class HGameInfo
{
public:
    
    HGameInfo(): mPoints(0), mLevel(1) {}
    virtual ~HGameInfo() {};
    
    int getPoints() { return mPoints; };
    void addPoints(int iPoints) { mPoints += iPoints; }
    void increaseLevel() { mLevel++; }
    int getLevel() { return mLevel; }
    
private:
    
    int mPoints;
    unsigned mLevel;
};

#endif /* HGameInfo_hpp */
