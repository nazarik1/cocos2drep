//
//  ActionScene.hpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/7/17.
//
//

#ifndef ActionScene_hpp
#define ActionScene_hpp

#include "cocos2d.h"
#include "MainScene.h"
#include "HCounter.h"
#include "HGameInfo.h"


class ActionScene : public cocos2d::Scene
{
public:
    
CREATE_FUNC_WITH_PHYSICS(ActionScene);
    virtual bool init();
    virtual bool initWithPhysics();
    
    void updateCharacterPosition();
    void createBottom();
    
    virtual void onExit();
    virtual void onEnter();
    
    // called once per frame
    virtual void update( float delta );
    void    updatePointsLabel();
    void    updateLevelLabel();
    
    // New elements
    void addCharacter();
    void addEnemy();
    void addCoin();
    void addBomb();
    void addGem();
    
private:
    
    bool onTouchBegin(cocos2d::Touch*, cocos2d::Event*);
    bool onContactBegan(cocos2d::PhysicsContact &contact);
    void fireThisElement(cocos2d::Sprite* iElement, float iDurationConstant);
    
    
    // Collisions
    bool handleCharacterCoinCollision(Node* iCoin);
    bool handleCharacterBombCollision(Node* iBomb);
    bool handleCharacterEnemyCollision(Node* iEnemy);
    bool handleBombObstacleCollision(Node* iBomb, Node* iObstacle);
    bool handleCharacterGemCollision(Node* iGem);
    void gameOver();
    
    cocos2d::EventListenerTouchOneByOne*    touchEventListener;
    cocos2d::EventListenerPhysicsContact*   contactListener;
    
    // Labels
    cocos2d::Label*     mLabel;
    cocos2d::Label*     mPointsLabel;
    cocos2d::Label*     mLevelLabel;
    
    cocos2d::Sprite*    mCharacter;
    HCounter            mCounter;
    HGameInfo           mGameInfo;
    bool                mCharacterDied;
    
    
};

#endif /* ActionScene_hpp */
