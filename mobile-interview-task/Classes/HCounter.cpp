//
//  HCounte.cpp
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#include "HCounter.h"

static const int kMaxCounterValue = 1000;

HCounter::HCounter(): count(0), numCycles(0)
{
    
}

HCounter::~HCounter()
{
    
}

void HCounter::increment()
{
    if(count == kMaxCounterValue)
    {
        count = 0;
        numCycles++;
    }
    else
        count++;
}

bool HCounter::isEnemyTime()
{
    if( count != 0 && count % 200 == 0)
        return true;
    
    return false;
}

bool HCounter::isCoinTime()
{
    if(count % 50 == 0)
        return true;
    
    return false;
}

bool HCounter::isBombTime()
{
    if(count !=0 && count % 250 == 0)
        return true;
    return false;
}