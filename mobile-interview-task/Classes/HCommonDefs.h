//
//  HCommonDefs.h
//  ExpJam
//
//  Created by Nazar Gavryshko on 2/8/17.
//
//

#ifndef HCommonDefs_h
#define HCommonDefs_h

#define CREATE_FUNC_WITH_PHYSICS(__TYPE__) \
static __TYPE__* createWithPhysics() \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->initWithPhysics()) \
{ \
pRet->autorelease(); \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}


enum ePhy_ElementCategory
{
    None = 0,
    eElement_Character  = (1 << 0),
    eElement_Obstacle   = (1 << 1),
    eElement_Coin       = (1 << 2),
    eElement_Enemy      = (1 << 3),
    eElement_Bomb       = (1 << 4),
    eElement_Gem        = (1 << 5),
    
};

static const char * kFontName_MarkerFelt        = "Marker Felt.ttf";
static const char * kButtonBlueImg              = "ButtonBlue.png";
static const char * kButtonGreenImg              = "ButtonGreen.png";

static const char * kMainCharacterImg           = "BlueBlob-Normal.png";
static const char * kMainCharacterFlatImg       = "BlueBlob-Flat.png";
static const char * kMainCharacterFallinglImg   = "BlueBlob-Falling.png";
static const char * kMainCharacterRedFlatImg    = "RedBlob-Flat.png";
static const char * kMainCharacterRedNormalImg  = "RedBlob-Normal.png";

static const char * kCoinImg            = "Coin.png";
static const char * kEnemyImg           = "OilBlob.png";
static const char * kBombImg            = "Bomb1.png";
static const char * kGrassObstacleImg   = "Leaf6.png";
static const char*  kGemImg             = "Gem.png";


static const char * kSmokeBrownImg  = "Smoke_Brown.png";
static const char * kSmokeOrangeImg = "Smoke_Orange.png";

static const char * kActionSceneBackgroundImg   = "background.jpg";
static const int    kBackgroundImageWidth       = 600;
static const int    kBackgroundImageHeight      = 349;

static const char * kPointsLabelCaption     = "Score: ";
static const char * kLevelLabelCaption      = "Level: ";

static const char * kStartGameMenuItem      = "Start";
static const char * kExitGameMenuItem       = "Exit";
static const char * kTryAgainGameMenuItem   = "TryAgain";
static const char * kGameOverText           = "Game Over!";

static const int kCoinPoints = 10;
static const int kGemPoints  = 50;

static const float kScaleFactor         = 0.5;
static const float kTouchimpulse        = 900;
static const float kVericalGravity      = -100.;
static const float kHorizontalGravity   = 0;

static const float kMainCharacterStartingPointX = 200.0;

#endif /* HCommonDefs_h */
